package com.example.cit.clinic_management_00;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cit.clinic_management_00.Model.LoginForm;

import org.json.JSONObject;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//import org.apache.commons.codec.binary.*;


public class MainScreen extends Activity  {
    Button b1,b2;
    EditText ed1,ed2;

    TextView tx1;
    int counter = 3;
    ImageView imgView;

    String urlString = "http://ec2-52-19-236-212.eu-west-1.compute.amazonaws.com/webapi/patients";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main_screen);

        b1=(Button)findViewById(R.id.button);
        ed1=(EditText)findViewById(R.id.editText);
        ed2=(EditText)findViewById(R.id.editText2);
        imgView=(ImageView) findViewById(R.id.imageView);
        imgView.setImageResource(R.drawable.iclauncherxxdpi);

        //b2=(Button)findViewById(R.id.button2);
        tx1=(TextView)findViewById(R.id.textView3);
        tx1.setVisibility(View.GONE);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ed1.getText().toString().equals("admin") &&

                        ed2.getText().toString().equals("admin")) {
                    Toast.makeText(getApplicationContext(), "Redirecting...",Toast.LENGTH_SHORT).show();
                    Thread thread = new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                request("http://ec2-52-19-236-212.eu-west-1.compute.amazonaws.com/webapi/patients");
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();

                }
                else{
                    Toast.makeText(getApplicationContext(), "Wrong Credentials",Toast.LENGTH_SHORT).show();

                    tx1.setVisibility(View.VISIBLE);
                    tx1.setBackgroundColor(Color.RED);
                    counter--;
                    tx1.setText(Integer.toString(counter));

                    if (counter == 0) {
                        b1.setEnabled(false);
                    }
                }
            }
        });

    }

    private void request(String urlString) {
        // TODO Auto-generated method stub
        //String URLString = "http://ec2-52-19-236-212.eu-west-1.compute.amazonaws.com/webapi/login";

        /*
        HttpURLConnection connection = null;

        StringBuffer chaine = new StringBuffer("");
        try{
            URL url = new URL(urlString);
            connection = (HttpURLConnection)url.openConnection();
            String basicAuth = "Basic " + Base64.encodeToString(("admin:admin").getBytes(), Base64.NO_WRAP);
            connection.setRequestProperty("Authorization", basicAuth);
            //connection.setRequestProperty("admin", "admin");
            //String userPassword = "admin:admin";
            //String encoding = Base64.encodeBase64(userPassword.getBytes();
            //connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.connect();

            System.out.println("*****CONNECTION: "+connection.getContent());
            /*InputStream inputStream = connection.getInputStream();

            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while ((line = rd.readLine()) != null) {
                chaine.append(line);
            }


        } catch (IOException e) {
            // writing exception to log
            e.printStackTrace();
        }

        return chaine;
        //return null;*/

        try {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            LoginForm loginForm = restTemplate.getForObject(urlString, LoginForm.class);
            System.out.println(loginForm);
        } catch (Exception e) {
            Log.e("MainActivity", e.getMessage(), e);
        }


        //new HttpRequestTask().execute();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, LoginForm> {
        @Override
        protected LoginForm doInBackground(Void... params) {
            try {

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                LoginForm loginForm = restTemplate.getForObject(urlString, LoginForm.class);
                return loginForm;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(LoginForm loginForm) {
            TextView loginFormUsername = (TextView) findViewById(R.id.editText);
            TextView greetingContentText = (TextView) findViewById(R.id.editText2);
            //loginFormIdText.setText(loginForm.getUsername());
            greetingContentText.setText(loginForm.getPassword());
        }

    }

}

